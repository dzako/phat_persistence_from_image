// Code to compute persistence homology using PHAT.
//Ordered simplices
//
// 2016
//
//

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <memory>
#include <algorithm>
#include <cstring>
#include <cmath>

// PHAT include files
#include "phat/representations/vector_vector.h"
#include <phat/algorithms/standard_reduction.h>
#include <phat/algorithms/twist_reduction.h>
#include <phat/compute_persistence_pairs.h>


class Simplex{
public:
  double value;
  int dim;
  int position; //index [0,...,N] //position in the list of all simplices of this dimension
  int globalPos; //index [0,...,N] //position in the whole list of simplices
  int listOfSimplices; //position in the list of all simplices
  double xCoord;
  double yCoord;
  
  std::list< Simplex* > boundary;
  std::list< Simplex* > coboundary;
  
  Simplex() : boundary(), coboundary() {
    value = HUGE_VAL; dim = 0; position = 0; globalPos= 0; listOfSimplices = -1;
    xCoord = 0; yCoord = 0;
  }
  
  Simplex(double v, int d, int p) : boundary(), coboundary(){
    value = v;
    dim = d;
    position = p;
    listOfSimplices = -1;
  }
    
  //sets value to min from coboundaries
  void evaluate(){
    value = HUGE_VAL;
    for (std::list<Simplex*>::iterator iterator = coboundary.begin(), end = coboundary.end(); iterator != end; ++iterator) {
      if( (**iterator).value < value ){
        value = (**iterator).value;
      }
    }
  }
  
  bool operator<(const Simplex& s2){ 
    const Simplex& s1 = *this;
    if(s1.value < s2.value) return true; 
    //returns ​true if the first argument is less than (i.e. is ordered before) the second
    if(s1.value > s2.value) return false;
    if(s1.value == s2.value){
      if(s1.dim < s2.dim) return true;
      if(s1.dim > s2.dim) return false;
      if(s1.dim == s2.dim){
        if(s1.position < s2.position) return true;
        if(s1.position > s2.position) return false;
        
        std::cerr << "s1.value==s2.value, s1.dim==s2.dim, s1.position==s2.position, WHICH SHOULDN'T HAPPEN!\n";
        exit(1);
      }
    }
  }

     
};


typedef std::unique_ptr<Simplex> pSimplex;

bool compare_ptrs(const pSimplex &p1, const pSimplex &p2){
  return *p1 < *p2;
}

           
    
std::ostream& operator<<(std::ostream& os, const Simplex& obj){
  return os << "(" << obj.value << ", " << obj.dim << ", " << obj.position << ", " << obj.globalPos << ", " << obj.xCoord << ", " << obj.yCoord << ")";
  //return os << "(" << obj.value << ", " << obj.dim << ", " << obj.position << ")";
}


std::vector<int> read_data(std::vector<int>& xcoords, std::vector<int>& ycoords, std::string input_file_name, std::string output_file_name_prefix, int data_sizes[]){
  bool verbose = true;  
  
  std::cout << "Input file name: " << input_file_name << std::endl;
  std::cout << "Output file name prefix: " << output_file_name_prefix << std::endl;
    

  const char *input_fname = input_file_name . c_str ();

   std::ifstream sol_file(input_fname);

   FILE * coords_file = fopen("coords.txt", "r");

   // Make sure the file was opened correctly
   if (!sol_file . is_open()) {
     std::cout << "Unable to open file: " << input_fname << std::endl;
     exit (1);
   }

   int d;
   sol_file >> d;
   if(d != 2){
     std::cerr << "Working only for 2D!\n";
     exit(1);
   }
   sol_file >> data_sizes[0];
   std::cout << "Read x_dim=" << data_sizes[0] << "\n";
   sol_file >> data_sizes[1];
   std::cout << "Read y_dim=" << data_sizes[1] << "\n";
   
   
   // Create a vector of {value, index} pairs
   std::vector< int > v_sol_indx;
   double v = 0.0;

   if (verbose) {
     std::cout << std::endl;
     std::cout << "Reading data ...";
   }

   int indx = 0, d1, d2;

   // Read the data from file
   while (sol_file >> v) {
     fscanf( coords_file, "%d %d", &d1, &d2 );
     xcoords.push_back(d1);
     ycoords.push_back(d2);
     v_sol_indx . push_back( v );  // Add pair
     ++indx;                                             // Increment index
   }

   sol_file . close();
   fclose( coords_file );

   if (verbose) {
     std::cout << " done!" << std::endl;
   }

   // Number of values read from file
   int num_values = v_sol_indx . size();

   // Check if size of data matches data read from file
   int total_data_size = data_sizes [0] * data_sizes [1];

   if (num_values != total_data_size) {
     std::cout << "Data sizes do not match! " << num_values << " " << total_data_size << std::endl;
     exit (1);
   }

   if (verbose) {
     std::cout << "Grid size: " << data_sizes [0] << " x " << data_sizes [1] << std::endl;
   }
   
   return v_sol_indx;
}



//adds simplices vertices
void generateVertices(std::vector<pSimplex>& out,  int data_sizes[]){
  const int N = (data_sizes[0] + 1) * (data_sizes[1] + 1) ;  
  int i, j;
                
  for( i = 0; i < N ; i ++){
    out[i] = pSimplex( new Simplex(HUGE_VAL, 0, i) );
  }
}


//adds simplices edges horizontal
void generateEdgesH(std::vector<pSimplex>& simplices, int Nvert, int data_sizes[], int &index){
  int i, j;
  const int N = (data_sizes[0] + 1) * (data_sizes[1] + 1) - 1;  
    
  int array_index = Nvert; // indexes simplices in the corresponding array of simplices 
  
  //adds horizontal edges first  
  for( i = 0; i < N ; i ++){
    if( i == 0 || i % (data_sizes[0]+1) != data_sizes[0] ){
      Simplex* s = new Simplex(HUGE_VAL, 1, index++) ;
  
      s->boundary .push_back( simplices[i].get() );
      s->boundary .push_back( simplices[i+1].get() );

      
      simplices[array_index] = pSimplex(s);           
            
      simplices[i]->coboundary.push_back( simplices[array_index].get() );
      simplices[i+1]->coboundary.push_back( simplices[array_index].get() );
      
      array_index++;
    }
  }  
}


//adds simplices edges vertical
void generateEdgesV(std::vector<pSimplex>& simplices, int NvertNedgesH, int data_sizes[], int &index){
  int i, j;
  const int N = (data_sizes[0] ) * (data_sizes[1] + 1);  
  
  int array_index = NvertNedgesH;
 
  //adds horizontal edges first  
  for( i = 0; i < N ; i ++){
    Simplex* s = new Simplex(HUGE_VAL, 1, index++);
    
    s->boundary .push_back( simplices[i].get() );
    s->boundary .push_back( simplices[ i + data_sizes[0] + 1 ].get() );


    simplices[array_index] = pSimplex(s);        
    
    simplices[ i ]->coboundary.push_back( simplices[array_index].get() );
    simplices[ i + data_sizes[0] + 1 ]->coboundary.push_back( simplices[array_index].get() );
    
    array_index++;
  }
}

//adds cubes 
void generateCubes(std::vector<pSimplex>& simplices, int Nvert, int NedgesH, std::vector<int>& values, std::vector<int>& xcoords, std::vector<int>& ycoords, int data_sizes[]){
  int i,j;
  const int N = values.size(); 
    
  int index = 0;
  
  for(int i = 0; i < N; i++){
    Simplex* s = new Simplex(values[i], 2, i) ;    
    
    s->xCoord = xcoords[i];
    s->yCoord = ycoords[i];

    s->boundary.push_back( simplices[ Nvert + i ].get() );
    s->boundary.push_back( simplices[ Nvert + i + data_sizes[0] ].get() );

    if(i != 0 && i % data_sizes[0] == 0) index++;

    s->boundary.push_back( simplices[ Nvert + NedgesH + i + index ].get() );
    s->boundary.push_back( simplices[ Nvert + NedgesH + i + 1 + index ].get() );


    simplices[Nvert + 2*NedgesH + i] = pSimplex( s );
    
    simplices[ Nvert + i ]->coboundary.push_back( simplices[ Nvert + 2*NedgesH + i ].get() );
    simplices[ Nvert + i + data_sizes[0] ]->coboundary.push_back( simplices[ Nvert + 2*NedgesH + i ].get() );
    
    simplices[ Nvert + NedgesH + i + index ]->coboundary.push_back( simplices[ Nvert + 2*NedgesH + i ].get() );
    simplices[ Nvert + NedgesH + i + 1 + index ]->coboundary.push_back( simplices[ Nvert + 2*NedgesH + i ].get() );
  }
  
}


//evaluates values of simplices
void evaluate(std::vector<pSimplex>& simplices, int Nvert, int Ncubes, int data_sizes[]){
  int N = simplices.size();
  int i;
  for(i = Nvert; i < N - Ncubes; i++){
    simplices[i]->evaluate();
  }

  for(i = 0; i < Nvert; i++){
    simplices[i]->evaluate();
  }

}


void updateCoords(int Nvert, int NedgesH, int NedgesV, std::vector<pSimplex>& simplices ){

  int i, j;
  const int startC = Nvert + NedgesH + NedgesV;

  //update coordinates for edges (boundaries of cubes)
  for(i = startC ; i < simplices.size() ; i++){
    j = 0;
    for (std::list<Simplex*>::iterator iterator = simplices[i]->boundary.begin(), end = simplices[i]->boundary.end(); iterator != end; ++iterator, ++j) {
      (*iterator)->xCoord = simplices[i]->xCoord;
      (*iterator)->yCoord = simplices[i]->yCoord;

    }
  }

  //update coordinates for vertices (boundaries of horizontal edges)
  int startVH = Nvert;
  int startVV = Nvert + NedgesH;

  for(i = startVH ; i < startVV ; i++){
    j = 0;
    for (std::list<Simplex*>::iterator iterator = simplices[i]->boundary.begin(), end = simplices[i]->boundary.end(); iterator != end; ++iterator, ++j) {
      (*iterator)->xCoord = simplices[i]->xCoord;
      (*iterator)->yCoord = simplices[i]->yCoord;

    }
  }

  //update coordinates for vertices (boundaries of vertical edges)
  for(i = startVV ; i < startC ; i++){
    j = 0;
    for (std::list<Simplex*>::iterator iterator = simplices[i]->boundary.begin(), end = simplices[i]->boundary.end(); iterator != end; ++iterator, ++j) {
      (*iterator)->xCoord = simplices[i]->xCoord;
      (*iterator)->yCoord = simplices[i]->yCoord;

    }
  }

}



int main(int argc, char *argv[])
{
  std::string in(argv[1], strlen(argv [1]));
  std::string out(argv[2], strlen(argv [2]));
  std::string outH0(argv[2], strlen(argv [2]));
  std::string outH1(argv[2], strlen(argv [2]));
    
  int output_type = atoi( argv[3] );

  std::vector<int> values, xcoords, ycoords;

  int data_sizes[2];
  
  std::ofstream outputH0 ( outH0.append("_H0") );
  std::ofstream outputH1 ( outH1.append("_H1") );


  values = read_data(xcoords, ycoords, in, out, data_sizes);

  for(int i = 0; i < values.size(); i++){
    std::cout << values[i] << " (" << xcoords[i] << "," << ycoords[i] << ")\n";
  }


  int Nvert = (data_sizes[0] + 1) * (data_sizes[1] + 1); 
  int NedgesH = ( (data_sizes[0] ) * (data_sizes[1] + 1) ); 
  int NedgesV = ( (data_sizes[0] ) * (data_sizes[1] + 1) );       
  int Ncubes = values.size();
  
  std::vector<pSimplex> simplices( Nvert + NedgesH + NedgesV + Ncubes );

  
  generateVertices(simplices, data_sizes);
  int index = 0;
  
  
  generateEdgesH(simplices, Nvert, data_sizes, index);
  generateEdgesV(simplices, Nvert + NedgesH, data_sizes, index);
  generateCubes( simplices, Nvert, NedgesH, values, xcoords, ycoords, data_sizes);
  
  evaluate(simplices, Nvert, Ncubes, data_sizes);

  updateCoords(Nvert, NedgesH, NedgesV, simplices);
  
  std::sort( simplices.begin(), simplices.end(), [](const pSimplex &p1, const pSimplex &p2){
    return *p1 < *p2;
  } );

  for(int i=0; i < simplices.size(); i++){
    simplices[i]->globalPos = i;
  }
  
  
  std::cout << "simplices:\n";
  for(int i=0; i < simplices.size(); i++){
      std::cout << i << ": " << *simplices[i] << "\n" ;
//     for (std::list<Simplex*>::iterator iterator = simplices[i]->boundary.begin(), end = simplices[i]->boundary.end(); iterator != end; ++iterator) {
//        std::cout << **iterator << ", ";
//      }std::cout << "\n";
  }
  
        
  //build a boundary matrix
  phat::boundary_matrix< phat::vector_vector > boundary_matrix;
  const int num_cols = simplices.size();
  boundary_matrix . set_num_cols(num_cols);
  std::vector< phat::index > col_vector;
  int col_indx = 0;
  
  // Add cells to the boundary matrix (filtration)
  for(int k0 = 0; k0 < num_cols; ++k0) {
    std::vector< phat::index > col_vector;
    col_vector . clear();    
    
    for (std::list<Simplex*>::iterator iterator = simplices[k0]->boundary.begin(), end = simplices[k0]->boundary.end(); iterator != end; ++iterator) {
      col_vector.push_back((*iterator)->globalPos);
    }        
    
    std::sort( col_vector.begin(), col_vector.end() );
    
    // Set current column (add empty column)
    const int col_dim = simplices[col_indx]->dim;
    boundary_matrix . set_dim( col_indx, col_dim);    
    boundary_matrix . set_col( col_indx++, col_vector );

  }  
  
  //boundary_matrix.save_ascii( out );
  
  phat::persistence_pairs pairs;
  
  phat::compute_persistence_pairs< phat::twist_reduction >( pairs, boundary_matrix );
  
  int NUM_PAIRS = pairs.get_num_pairs();

  if( output_type == 0 ){
    for( int i =0; i < NUM_PAIRS; i++){
      if( (int)boundary_matrix . get_dim( pairs.get_pair(i).first ) == 0 )
        if( simplices[pairs.get_pair(i).first]->value != simplices[pairs.get_pair(i).second]->value )
          outputH0 <<  simplices[pairs.get_pair(i).first]->value << " " << simplices[pairs.get_pair(i).second]->value << "\n";

      if( (int)boundary_matrix . get_dim( pairs.get_pair(i).first ) == 1 )
        if( simplices[pairs.get_pair(i).first]->value != simplices[pairs.get_pair(i).second]->value )
          outputH1 <<  simplices[pairs.get_pair(i).first]->value << " " << simplices[pairs.get_pair(i).second]->value << "\n";

    }
  }
  if( output_type == 1 ){
    for( int i =0; i < NUM_PAIRS; i++){
        if( (int)boundary_matrix . get_dim( pairs.get_pair(i).first ) == 0 )

          if( simplices[pairs.get_pair(i).first]->value != simplices[pairs.get_pair(i).second]->value)
            outputH0 << simplices[pairs.get_pair(i).first]->xCoord << " " << simplices[pairs.get_pair(i).first]->yCoord << " "
            << simplices[pairs.get_pair(i).second]->xCoord << " " << simplices[pairs.get_pair(i).second]->yCoord << "\n";

        if( (int)boundary_matrix . get_dim( pairs.get_pair(i).first ) == 1 )

          if( simplices[pairs.get_pair(i).first]->value != simplices[pairs.get_pair(i).second]->value)
            outputH1 << simplices[pairs.get_pair(i).first]->xCoord << " " << simplices[pairs.get_pair(i).first]->yCoord << " "
            << simplices[pairs.get_pair(i).second]->xCoord << " " << simplices[pairs.get_pair(i).second]->yCoord << "\n";
      }
  }
  if( output_type == 2 ){
    for( int i =0; i < NUM_PAIRS; i++){
      if( (int)boundary_matrix . get_dim( pairs.get_pair(i).first ) == 0 )

        if( simplices[pairs.get_pair(i).first]->value != simplices[pairs.get_pair(i).second]->value )
          outputH0 <<  simplices[pairs.get_pair(i).first]->value << "(" << simplices[pairs.get_pair(i).first]->dim << "," << simplices[pairs.get_pair(i).first]->xCoord << "," << simplices[pairs.get_pair(i).first]->yCoord << ") "
          << simplices[pairs.get_pair(i).second]->value << "(" << simplices[pairs.get_pair(i).second]->dim << "," << simplices[pairs.get_pair(i).second]->xCoord << "," << simplices[pairs.get_pair(i).second]->yCoord << ")" << "\n";

      if( (int)boundary_matrix . get_dim( pairs.get_pair(i).first ) == 1 )

        if( simplices[pairs.get_pair(i).first]->value != simplices[pairs.get_pair(i).second]->value )
          outputH1 <<  simplices[pairs.get_pair(i).first]->value << "(" << simplices[pairs.get_pair(i).first]->dim << "," << simplices[pairs.get_pair(i).first]->xCoord << "," << simplices[pairs.get_pair(i).first]->yCoord << ") "
          << simplices[pairs.get_pair(i).second]->value << "(" << simplices[pairs.get_pair(i).second]->dim << "," << simplices[pairs.get_pair(i).second]->xCoord << "," << simplices[pairs.get_pair(i).second]->yCoord << ")" << "\n";

    }
  }

  
  outputH0.close();
  outputH1.close();

//  std::cout << "edgesH:\n";
//  for(int i=0; i < edgesH.size(); i++)
////      std::cout << edgesH[i] << ", " << *(edgesH[i].coboundary.front()) << ", " << *(edgesH[i].coboundary.back()) << "\n";
//  {
//    std::cout << edgesH[i] << "\n";
//    
//    /*for (std::list<Simplex*>::iterator iterator = edgesH[i].boundary.begin(), end = edgesH[i].boundary.end(); iterator != end; ++iterator) {
//        std::cout << **iterator << ", ";
//    }*/
//    std::cout << "cobd:";
//    
//    for (std::list<Simplex*>::iterator iterator = edgesH[i].coboundary.begin(), end = edgesH[i].coboundary.end(); iterator != end; ++iterator) {
//        std::cout << **iterator << ", ";
//    }
//    std::cout << "\n";
//  }  
//  std::cout << "\n\n";  
//  
//  std::cout << "edgesV:\n";
//  for(int i=0; i < edgesV.size(); i++)
////        std::cout << edgesV[i] << ", " << *(edgesV[i].coboundary.front()) << ", " <<*(edgesV[i].coboundary.back()) << "\n";
//  {
//    std::cout << edgesV[i] << "\n";
//    
//    /*for (std::list<Simplex*>::iterator iterator = edgesV[i].boundary.begin(), end = edgesV[i].boundary.end(); iterator != end; ++iterator) {
//       std::cout << **iterator << ", ";
//    }*/
//    std::cout << "cobd:";
//    
//    for (std::list<Simplex*>::iterator iterator = edgesV[i].coboundary.begin(), end = edgesV[i].coboundary.end(); iterator != end; ++iterator) {
//       std::cout << **iterator << ", ";
//    }
//    std::cout << "\n";
//  }
//  
//  std::cout << "\n\n";
//  
//  std::cout << "vertices:\n";
//  for(int i=0; i < vertices.size(); i++){
//    std::cout << vertices[i] << ", " ;
//  
//    for (std::list<Simplex*>::iterator iterator = vertices[i].coboundary.begin(), end = vertices[i].coboundary.end(); iterator != end; ++iterator) {
//        std::cout << **iterator << ", ";
//    }
//    std::cout << "\n";
//  }
//  
//  std::cout << "\n\n";
//  
//  std::cout << "cubes:\n";
//  for(int i=0; i < cubes.size(); i++){
//      std::cout << cubes[i] << ", " ;
//    
//      for (std::list<Simplex*>::iterator iterator = cubes[i].boundary.begin(), end = cubes[i].boundary.end(); iterator != end; ++iterator) {
//         std::cout << **iterator << ", ";
//      }
//      std::cout << "\n";
//    }
//  
//  
//  std::cout << "\n\n";
  
}
